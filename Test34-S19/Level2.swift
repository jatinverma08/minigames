//
//  GameScene.swift
//  Test34-S19
//
//  Created by MacStudent on 2019-06-19.
//  Copyright © 2019 rabbit. All rights reserved.
//

import SpriteKit
import GameplayKit

class Level2: SKScene {
    let dino = SKSpriteNode(imageNamed:"dino")
    var nextLevelButton:SKLabelNode!
    var ground:SKNode!
 
    
    var timeOfLastUpdate:TimeInterval = 0
    var dt: TimeInterval = 0
    override func didMove(to view: SKView) {
               self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        
        dino.position = CGPoint(x:self.size.width - 200,
                                 y:self.size.height/2)
        // setup enemy movement
        // move to (w/2, 0)
        let m1 = SKAction.move(
            to: CGPoint(
                x:self.size.width/2,
                y:400),
            duration: 2)
        
        // move to (0,h/2)
        let m2 = SKAction.move(
            to: CGPoint(
                x:200,
                y:self.size.height/2),
            duration: 2)
        
        // move to (w, h/2)
        let m3 = SKAction.move(
            to: CGPoint(
                x:self.size.width - 200,
                y:self.size.height/2),
            duration: 2)
        
        
        let sequence:SKAction = SKAction.sequence([m1, m2, m1, m3])
        dino.run(SKAction.repeatForever(sequence))

    }
    func spawndino() {
        let dino = SKSpriteNode(imageNamed:"dino")
        
        // put sand at a random (x,y) position
        let x = self.size.width/2
        let y = self.size.height - 100
        dino.position.x = x
        dino.position.y = y
        
        // add physics
        dino.physicsBody = SKPhysicsBody(circleOfRadius: dino.size.width / 2)
        self.dino.physicsBody?.affectedByGravity = true
        
        addChild(dino)
    }
    
    func makeline(xPosition:CGFloat, yPosition:CGFloat) {
        
        // 1. create an orange sprite
        let line = SKSpriteNode(imageNamed: "line")
        
        line.position.x = xPosition;
        line.position.y = yPosition;
        
        addChild(line)
    }
    
    override func update(_ currentTime: TimeInterval) {
        // make new dino every 30ms
        self.dt = currentTime - timeOfLastUpdate
        
        if (self.dt >= 3) {
            timeOfLastUpdate = currentTime
            self.spawndino()
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let mousePosition = touch.location(in:self)
        
        // 2. make an line in the same position as mouse click
        self.makeline(
            xPosition:mousePosition.x,
            yPosition:mousePosition.y)
        
        
        
  
        
       
        
        }
        
    }

