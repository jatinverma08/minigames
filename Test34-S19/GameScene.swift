//
//  GameScene.swift
//  Test34-S19
//
//  Created by MacStudent on 2019-06-19.
//  Copyright © 2019 rabbit. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var nextLevelButton:SKLabelNode!
    let line = SKSpriteNode(imageNamed:"line")
    
    
    override func didMove(to view: SKView) {
        print("This is level 1")
        self.nextLevelButton = self.childNode(withName: "nextLevelButton") as! SKLabelNode
        
        
        
            // setup mouse movement on leg 
        
        // move to hair 2
        let m1 = SKAction.move(
            to: CGPoint(
                x:self.size.width/2,
                y:400),
            duration: 2)
        
        // move to hair 3
        let m2 = SKAction.move(
            to: CGPoint(
                x:200,
                y:self.size.height/2),
            duration: 2)
        
         // move to hair 4
        let m3 = SKAction.move(
            to: CGPoint(
                x:self.size.width - 200,
                y:self.size.height/2),
            duration: 2)
        // move to hair 5
        let m4 = SKAction.move(
            to: CGPoint(
                x:self.size.width - 200,
                y:self.size.height/2),
            duration: 2)
        
        
        let sequence:SKAction = SKAction.sequence([m1, m2, m3, m4])
       line.run(SKAction.repeatForever(sequence))
    }
    
    func makeline(xPosition:CGFloat, yPosition:CGFloat) {
        
        // 1. create an orange sprite
        let line = SKSpriteNode(imageNamed: "line")
        
        line.position.x = xPosition;
        line.position.y = yPosition;
        
        addChild(line)
    }
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        
        
        let location = touch!.location(in:self)
        let node = self.atPoint(location)
        
        
        
        
        // MARK: Switch Levels
        if (node.name == "nextLevelButton") {
            let scene = SKScene(fileNamed:"Level2")
            if (scene == nil) {
                print("Error loading level")
                return
            }
            else {
                scene!.scaleMode = .aspectFill
                view?.presentScene(scene!)
            }
        }
        
    }
}
